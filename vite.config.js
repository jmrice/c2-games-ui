import { plugin as markdownPlugin } from 'vite-plugin-markdown';
import { sveltekit } from '@sveltejs/kit/vite';
import MarkdownIt from './src/lib/util/markdown-it';

/** @type {import('vite').UserConfig} */
export default {
  server: {
    port: 3000,
    server: {
      fs: {
        allow: ['./static']
      }
    }
  },
  plugins: [
    sveltekit(),
    markdownPlugin({
      mode: ['toc', 'html'],
      markdownIt: MarkdownIt()
    })
  ]
};
