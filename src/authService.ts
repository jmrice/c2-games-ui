import Keycloak from 'keycloak-js';
import { isAuthenticated, keycloakClient, keycloakToken, keycloakTokenParsed, user, userId } from './stores/authStore';
import { goto } from '$app/navigation';
import { whoAmI } from './lib/util/apiHelpers';

export async function createClient() {
  const kc = new Keycloak({
    url: import.meta.env.VITE_KEYCLOAK_URL,
    realm: import.meta.env.VITE_KEYCLOAK_REALM,
    clientId: import.meta.env.VITE_KEYCLOAK_CLIENT_ID
  });

  // update keycloakClient store with the new client. We do this early so that
  // subscribers of keycloakClient have the updated client when this function exits
  keycloakClient.set(kc);
  await kc.init({
    onLoad: 'check-sso',
    silentCheckSsoFallback: true,
    silentCheckSsoRedirectUri: `${window.location.origin}/ssoRedirect`
  });
  await updateStores(kc);

  if (!kc.authenticated) {
    return kc;
  }

  // Set an interval to automatically refresh the token. This checks every 30 seconds
  // for if the token will expire within the next 45 seconds
  // todo this might stack
  setInterval(async () => {
    try {
      console.debug('Keycloak: Attempting to update keycloak token');
      const refreshed = await kc.updateToken(45);
      if (refreshed) {
        console.debug('Keycloak: Token was successfully refreshed');
        await updateStores(kc);
      } else console.debug('Keycloak: Token is still valid');
    } catch (err) {
      console.error(`Keycloak: Failed to refresh the token, or the session has expired ${err}`);
    }
  }, 30000);

  return kc;
}

export async function login(client: Keycloak) {
  try {
    await client.login();
    await updateStores(client);
  } catch (err) {
    console.error(err);
  }
}

export async function logout(client: Keycloak) {
  // Get to an unauthenticated page first, then logout and clear the stores
  await goto('/');
  await client.logout();
  await updateStores(client);
}

async function updateStores(client: Keycloak) {
  keycloakToken.set(client.token || null);
  keycloakTokenParsed.set(client.tokenParsed || null);
  isAuthenticated.set(client.authenticated);
  if (client.authenticated) {
    // update user ID in tracking-api db
    const { id } = await whoAmI();
    userId.set(id);
    user.set({
      id: client.tokenParsed?.sub,
      firstName: client.tokenParsed?.given_name,
      lastName: client.tokenParsed?.family_name,
      email: client.tokenParsed?.email
    });
  } else {
    userId.set(null);
    user.set(null);
  }
}

export async function register(client: Keycloak) {
  try {
    await client.register();
  } catch (err) {
    console.error(err);
  }
}
