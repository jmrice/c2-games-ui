import { writable } from 'svelte/store';
import { lightboxStateDefault } from '../lib/constants/lightboxStateDefault';

export const lightboxState = writable(lightboxStateDefault);
