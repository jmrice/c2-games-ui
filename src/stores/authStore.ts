import { writable, type Writable } from 'svelte/store';
import type Keycloak from 'keycloak-js';
import type { KeycloakTokenParsed } from 'keycloak-js';

export type AuthUser = {
  id?: string;
  firstName: string;
  lastName: string;
  email: string;
};

export const isAuthenticated: Writable<boolean | undefined> = writable(false);
export const keycloakClient: Writable<Keycloak> = writable();
export const keycloakToken: Writable<string | null> = writable(null);
export const keycloakTokenParsed: Writable<KeycloakTokenParsed | null> = writable(null);
export const user: Writable<AuthUser | null> = writable(null);
export const userId: Writable<number | null> = writable(null);

keycloakClient.subscribe((client) => console.debug('new KC client', client));
