import MarkdownIt from 'markdown-it';

/**
 * Get a preconfigured instance of MarkdownIt
 */
export default () => {
  // Initialize Markdown Renderer
  const markdownIt = MarkdownIt({
    linkify: true
  });

  const headerCssMap = [
    'md:text-5xl', // h1
    'md:text-4xl', // h2
    'md:text-3xl', // h3
    'md:text-2xl', // h4
    'md:text-xl', // h5
    'md:text-lg' // h6
  ];

  // Add custom classes to HTML generated from Markdown.
  // markdownIt.block.ruler and markdownIt.inline.ruler are also available
  markdownIt.core.ruler.push('c2games', (state) => {
    // Iterate all tokens and handle special cases appropriately
    for (const token of state.tokens) {
      if (token.type === 'heading_open') {
        // Add Tailwind text-Nxl classes to headers
        const level = parseInt(token.tag[1]);
        token.attrJoin('class', headerCssMap[level]);
        // add padding to headers
        token.attrJoin('class', 'py-5');
        // Small screens get weird with super large text,
        // so reduce text size of all headers on small screens
        token.attrJoin('class', 'text-2xl');
      } else if (token.type === 'ordered_list_open') {
        // add number before ordered lists
        token.attrJoin('class', 'list-decimal');
        token.attrJoin('class', 'list-inside');
        token.attrJoin('class', 'md-ol');
      } else if (token.type === 'bullet_list_open') {
        // add bullet before unordered lists
        token.attrJoin('class', 'list-disc');
        token.attrJoin('class', 'list-inside');
      } else if (token.type === 'paragraph_open') {
        // add padding to paragraphs
        token.attrJoin('class', 'py-1');
        // console.log(token);
      } else if (token.tag === 'li') {
        // add class for overriding the p tag that is added to list items
        token.attrJoin('class', 'md-li');
        // add padding to list items (including nested list items)
        token.attrJoin('class', 'py-1');
      }
    }
  });

  return markdownIt;
};
