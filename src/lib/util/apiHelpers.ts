import { keycloakToken } from '$stores/authStore';

export const apiUrl = import.meta.env.VITE_API_URL;

async function token() {
  return await new Promise((resolve) => keycloakToken.subscribe(resolve));
}

export interface User {
  id: number;
  email: string;
  enabled: boolean;

  keycloakSid: string;
  createdByUserId: number;
  createdDate: string;
  updatedByUserId: number;
  updatedDate: string;
}

interface ResponseWithMessage extends Response {
  message: string;
}

export function isFetchResponse(obj: unknown): obj is Response {
  return !!(obj && typeof obj === 'object' && Object.prototype.hasOwnProperty.call(obj, 'json'));
}

export function isFetchResponseWithMessage(obj: unknown): obj is ResponseWithMessage {
  return !!(obj && typeof obj === 'object' && Object.prototype.hasOwnProperty.call(obj, 'message'));
}

export function hasToString(obj: unknown): obj is { toString(): string } {
  return Object.prototype.hasOwnProperty.call(obj, 'toString');
}

export function tryParseApiError(e: unknown): string {
  if (!e) return 'Unknown Error Occurred';

  if (!isFetchResponseWithMessage(e)) {
    // error isn't a known type, try to call toString(), otherwise return as is
    return hasToString(e) ? e?.toString() : e;
  }

  try {
    // Parse JSON response
    const json = JSON.parse(e.message);
    // If JSON response is in format {message: string}, return .message (a common API response)
    if (json.message) return json.message;
    // Otherwise, just return JSON as is
    return json;
  } catch {
    // We failed to parse JSON, try to return a string instead
    return e.toString();
  }
}

export async function whoAmI(): Promise<User> {
  const res = await fetch(apiUrl + '/v1/auth/whoami', {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

export async function getUserById(userId: number): Promise<User> {
  return getAPI<User>(apiUrl + `/v1/users/${userId}`);
}

export async function getUserByEmail(email: string): Promise<User> {
  return getAPI<User>(apiUrl + `/v1/users/find`, { email });
}

export interface UserBadge {
  badgeId: number;
  earnedOn: Date;
  title: string;
  description: string;
  imageUrl: string;
}

async function _getApiData(url: string, params?: Record<string, string | number>): Promise<Response> {
  const apiUrl = new URL(url);
  if (params) {
    for (const k of Object.keys(params)) {
      apiUrl.searchParams.append(k, params[k]?.toString());
    }
  }

  return await fetch(apiUrl, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
}

async function getAPI<T>(url: string, params?: Record<string, string | number>): Promise<T> {
  const res = await _getApiData(url, params);
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

async function getAPIFile(url: string, filename: string, params?: Record<string, string | number>) {
  const res = await _getApiData(url, params);
  if (!res.ok) throw new Error(await res.text());

  const blob = await res.blob();
  const fileUrl = window.URL.createObjectURL(blob);

  // Stack Overflow hack to get the file contents as a download
  // https://stackoverflow.com/questions/3749231/download-file-using-javascript-jquery
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = fileUrl;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  window.URL.revokeObjectURL(url);
}

export async function getTeamSummaryExport(filters?: TeamFilter) {
  return getAPIFile(apiUrl + '/v1/teams/export/summary', 'team-summary.csv', filters);
}

export async function getBadge(id: number) {
  const res = await fetch(apiUrl + `/v1/badges/${id}`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return res.json();
  }
}

export async function getBadgesForUser(userId: number) {
  const res = await fetch(apiUrl + `/v1/users/${userId}/badges`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    const badges = await res.json();
    return Promise.all<UserBadge>(
      badges.map(
        async (badge: any) =>
          ({ badgeId: badge.id, earnedOn: new Date(badge.earnedOn), ...(await getBadge(badge.badgeId)) } as UserBadge)
      )
    );
  } else {
    throw new Error(await res.text());
  }
}

export interface UserChecklistItem {
  checklistItemId: number;
  completedOn: Date | null;
  title: string;
  description: string;
}

export async function getUserChecklistItem(id: number) {
  const res = await fetch(apiUrl + `/v1/user-checklist-items/${id}`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return res.json();
  }
}

export async function getUserChecklistItemsForUser(userId: number) {
  const res = await fetch(apiUrl + `/v1/users/${userId}/user-checklist-items`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    const items = await res.json();
    return Promise.all<UserChecklistItem>(
      items.map(
        async (item: any) =>
          ({
            checklistItemId: item.userChecklistItemId,
            completedOn: item.completedOn ? new Date(item.completedOn) : null,
            ...(await getUserChecklistItem(item.userChecklistItemId))
          } as UserChecklistItem)
      )
    );
  } else {
    throw new Error(await res.text());
  }
}

export interface UserAchievement {
  achievementId: number;
  earnedOn: Date;
  title: string;
  description: string;
}

export async function getAchievement(id: number) {
  const res = await fetch(apiUrl + `/v1/achievements/${id}`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return res.json();
  }
}

export async function getAchievementsForUser(userId: number) {
  const res = await fetch(apiUrl + `/v1/users/${userId}/achievements`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    const items = await res.json();
    return Promise.all<UserAchievement>(
      items.map(
        async (item: any) =>
          ({
            achievementId: item.achievementId,
            earnedOn: new Date(item.earnedOn),
            ...(await getAchievement(item.achievementId))
          } as UserAchievement)
      )
    );
  } else {
    throw new Error(await res.text());
  }
}

export interface UserMessage {
  messageId: number;
  content: string;
  announcedOn: Date;
  seenOn: Date | null;
}

export async function getMessagesForUser(userId: number): Promise<UserMessage[]> {
  const res = await fetch(apiUrl + `/v1/users/${userId}/messages`, {
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    const items = await res.json();
    return items;
  } else {
    throw new Error(await res.text());
  }
}

export interface EventTeamUser {
  id: number;
  eventTeamId: number;
  userId: number;
  user?: User;

  enabled: boolean;
  addedOn: string;
  createdByUserId: number;
  updatedByUserId: number;
  createdDate: string;
  updatedDate: string;
}

export type TeamFilter = {
  minMembers?: number;
  maxMembers?: number;
  eventId?: number;
  region?: Region;
};

export async function getAllTeams(params?: TeamFilter) {
  return getAPI<EventTeam[]>(apiUrl + '/v1/teams', params);
}

export async function getEventTeamUsersForTeam(teamId: number): Promise<EventTeamUser[]> {
  return getAPI<EventTeamUser[]>(apiUrl + `/v1/event-teams/${teamId}/users`);
}

export async function getEventTeamUsersForUser(userId: number): Promise<EventTeamUser[]> {
  return getAPI<EventTeamUser[]>(apiUrl + `/v1/event-teams`, { userId: `${userId}` });
}

export async function getEventTeamsByDomain(): Promise<EventTeam[]> {
  return getAPI<EventTeam[]>(apiUrl + `/v1/events/teams/domain`);
}

export async function createEventTeamUser(teamId: number, userId: number): Promise<EventTeamUser> {
  const res = await fetch(apiUrl + `/v1/event-teams/${teamId}/users/${userId}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${await token()}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ addedOn: new Date().toISOString() })
  });
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

export async function deleteEventTeamUser(id: number) {
  const res = await fetch(apiUrl + `/v1/event-team-users/${id}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

export async function deleteEventTeam(teamId: number) {
  const res = await fetch(apiUrl + `/v1/events/teams/${teamId}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${await token()}`
    }
  });
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

export interface EventTeam {
  id: number;
  eventId: number;
  name: string;
  createdByUserId: number;

  eventTeamUsers?: EventTeamUser[];
  event?: Event;
}

export interface CreateTeamDto {
  name: string;
}

export async function getEventTeamById(teamId: number): Promise<EventTeam> {
  return getAPI<EventTeam>(apiUrl + `/v1/events/teams/${teamId}`);
}

export async function getTeamCountByEventId(eventId: number): Promise<number> {
  return (await getAPI<{ count: number }>(apiUrl + `/v1/events/${eventId}/teams/count`)).count;
}

export async function createEventTeam(eventId: number, dto: CreateTeamDto, institutionId?: number): Promise<EventTeam> {
  let url = `${apiUrl}/v1/events/${eventId}/teams?`;
  if (institutionId) url += new URLSearchParams({ institutionId: institutionId.toString() });
  const res = await fetch(url, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${await token()}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(dto)
  });
  if (res.ok) {
    return await res.json();
  } else {
    throw new Error(await res.text());
  }
}

export interface Event {
  id: number;
  name: string;
  startsOn: Date;
  regions: Region[];
  rules?: string;
  enabled: boolean;
}

export async function getEvents(params?: { title?: string; region?: Region }): Promise<Event[]> {
  const events = await getAPI<Event[]>(apiUrl + `/v1/events/`, params);
  return events.map((event) => ({ ...event, startsOn: new Date(event.startsOn) }));
}

export interface EventChecklistItem {
  id: number;
  title: string;
  description: string;
}

export interface EventTeamChecklistItem extends EventChecklistItem {
  eventId: number;
  eventTeamId: number;
  eventChecklistItemId: number;
  completedOn: Date;
}

export async function getEventChecklistItem(id: number): Promise<EventChecklistItem> {
  return getAPI(apiUrl + `/v1/event-checklist-items/${id}`);
}

export async function getEventTeamChecklistItems(teamId: number): Promise<EventTeamChecklistItem[]> {
  const teamItems: EventTeamChecklistItem[] = await getAPI(apiUrl + `/v1/event-teams/${teamId}/event-checklist-items`);
  return Promise.all(
    teamItems.map((teamItem) =>
      getEventChecklistItem(teamItem.eventChecklistItemId).then((item) => ({ ...item, ...teamItem }))
    )
  );
}

export interface Institution {
  id: number;
  name: string;
  domain: string;
  state: State;
  region: Region;
  updatedDate: Date;
  createdDate: Date;
}

export async function getInstitutions(params?: { name?: string; domain?: string; state?: State; region?: Region }) {
  return await getAPI<Institution[]>(apiUrl + `/v1/institutions/`, params);
}

export type Region = 'southeast' | 'northeast' | 'midwest' | 'southwest' | 'northwest';
export type State =
  | 'Alabama'
  | 'Alaska'
  | 'Arizona'
  | 'Arkansas'
  | 'California'
  | 'Colorado'
  | 'Connecticut'
  | 'Delaware'
  | 'District of Columbia'
  | 'Florida'
  | 'Georgia'
  | 'Hawaii'
  | 'Idaho'
  | 'Illinois'
  | 'Indiana'
  | 'Iowa'
  | 'Kansas'
  | 'Kentucky'
  | 'Louisiana'
  | 'Maine'
  | 'Maryland'
  | 'Massachusetts'
  | 'Michigan'
  | 'Minnesota'
  | 'Mississippi'
  | 'Missouri'
  | 'Montana'
  | 'Nebraska'
  | 'Nevada'
  | 'New Hampshire'
  | 'New Jersey'
  | 'New Mexico'
  | 'New York'
  | 'North Carolina'
  | 'North Dakota'
  | 'Ohio'
  | 'Oklahoma'
  | 'Oregon'
  | 'Pennsylvania'
  | 'Puerto Rico'
  | 'Rhode Island'
  | 'South Carolina'
  | 'South Dakota'
  | 'Tennessee'
  | 'Texas'
  | 'Utah'
  | 'Vermont'
  | 'Virginia'
  | 'Washington'
  | 'West Virginia'
  | 'Wisconsin'
  | 'Wyoming';
