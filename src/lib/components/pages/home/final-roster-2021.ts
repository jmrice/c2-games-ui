import northwest1 from '$lib/assets/images/teams/eastern_washington.png';
import northwest2 from '$lib/assets/images/teams/brigham-young.png';
import northwest3 from '$lib/assets/images/teams/UniversityOfDenver_Signature.png';

import northeast1 from '$lib/assets/images/teams/team7.svg';
import northeast2 from '$lib/assets/images/teams/team4.png';
import northeast3 from '$lib/assets/images/teams/syracuse-college.png';

import southwest1 from '$lib/assets/images/teams/utulsa.png';

import midwest1 from '$lib/assets/images/teams/IIT_Logo.png';

import southeast1 from '$lib/assets/images/teams/UCF.png';
import southeast2 from '$lib/assets/images/teams/Kennesaw.png';

interface Team {
  name: string;
  imageUrl: string;
}

export const winner: Team = {
  name: 'Illinois Institute of Technology',
  imageUrl: midwest1
};

export default {
  Northwest: [
    {
      name: 'Eastern Washington University',
      imageUrl: northwest1
    },
    {
      name: 'Brigham Young University',
      imageUrl: northwest2
    },
    {
      name: 'University of Denver',
      imageUrl: northwest3
    }
  ] as Team[],
  Northeast: [
    {
      name: 'Rhode Island Rams',
      imageUrl: northeast1
    },
    {
      name: 'Liberty University',
      imageUrl: northeast2
    },
    {
      name: 'Syracuse',
      imageUrl: northeast3
    }
  ] as Team[],
  Southwest: [
    {
      name: 'University of Tulsa',
      imageUrl: southwest1
    }
  ] as Team[],
  Midwest: [
    {
      name: 'Illinois Institute of Technology',
      imageUrl: midwest1
    }
  ] as Team[],
  Southeast: [
    {
      name: 'University of Central Florida',
      imageUrl: southeast1
    },
    {
      name: 'Kennesaw State University',
      imageUrl: southeast2
    }
  ] as Team[]
};
