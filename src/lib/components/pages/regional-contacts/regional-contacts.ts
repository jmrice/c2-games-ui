export default [
  {
    region: 'North Eastern',
    contacts: [
      {
        name: 'John Watkins',
        email: 'john.watkins@sunywcc.edu'
      }
    ]
  },
  {
    region: 'South Eastern',
    contacts: [
      {
        name: 'Anthony Pinto',
        email: 'apinto@uwf.edu'
      }
    ]
  },
  {
    region: 'Mid Western',
    contacts: [
      {
        name: 'Lonnie Decker',
        email: 'mdldecker@davenport.edu'
      },
      {
        name: 'Stanley Kostka',
        email: 'kostkas@morainevalley.edu'
      }
    ]
  },
  {
    region: 'South Western',
    contacts: [
      {
        name: 'Nancy Jones',
        email: 'njones2@nu.edu'
      },
      {
        name: 'Kim Muschalek',
        email: 'kmuschalek@alamo.edu'
      },
      {
        name: 'Andres Vianes',
        email: 'avianes3@alamo.edu'
      }
    ]
  },
  {
    region: 'North Western',
    contacts: [
      {
        name: 'Stu Steiner',
        email: 'ssteiner@ewu.edu'
      },
      {
        name: 'Albert Tay',
        email: 'albert_tay@byu.edu'
      },
      {
        name: 'Gretchen Bliss',
        email: 'gbliss@uccs.edu'
      },
      {
        name: 'Nina Amey',
        email: 'namey@uccs.edu'
      }
    ]
  }
];
