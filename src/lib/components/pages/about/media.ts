export type MediaPostData = {
  video: string;
  title: string;
  description: string;
  tags: string[];
};

export const twitchPosts: MediaPostData[] = [
  {
    video: 'https://www.youtube.com/embed/6jVA3J2_JYI',
    title: '2022 November Twitch Stream',
    description:
      'NCAE Cyber Games contributors James Rice and Mike Lisi update the community on a number of details such as: \n' +
      '- New website and team registration\n' +
      '- CTF challenge walkthrough\n' +
      '- Questionable Cyber Strategies\n' +
      '- Meet some former/future NCAE Cyber Games competitors\n' +
      '- More!',
    tags: ['twitch', 'live']
  },
  {
    video: 'https://www.youtube.com/embed/Ju3zFF6aYgQ',
    title: '2022 October Twitch Stream',
    description:
      'NCAE Cyber Games contributors James Rice and Mike Lisi update the community on a number of details such as: \n' +
      '- Overview of last season\n' +
      '- CTF challenge walkthrough (non-technical and technical)\n' +
      '- Questionable Cyber Strategies\n' +
      '- Meet some former/future NCAE Cyber Games competitors\n' +
      '- More!',
    tags: ['twitch', 'live']
  }
];

export const mtpPosts: MediaPostData[] = [
  {
    video: 'https://www.youtube.com/embed/i5gjAJ_d8Vk',
    title: 'Mohawk Valley Community College - Cyberhawks',
    description: "Meet The Players of the Mohawk Valley Community College's cyber security club, the **Cyberhawks**.",
    tags: ['Meet the Players', 'MVCC', 'cyberhawks', 'Northeast Region']
  },
  {
    video: 'https://www.youtube.com/embed/XcDWRJ0T-TI',
    title: 'Dakota State University',
    description: 'Meet The Players with Dakota State Universities 2022 team leader, Ben.',
    tags: ['Meet the Players', 'DSU', 'Northwest Region']
  }
];

export const ctfPosts: MediaPostData[] = [
  {
    video: 'https://www.youtube.com/embed/Q9Zr3LAKxYs',
    title: 'Crypto 201',
    description: `
Follow along as CTF Creator Mike Lisi and James Rice solve a Cryptography CTF challenge from our live sandbox, 
Crypto 201. Mike also gives a background about Cryptography, and why Cryptography is important in Cyber Security.`,
    tags: ['CTF', 'Crypto', 'sandbox', 'live']
  },
  {
    video: 'https://www.youtube.com/embed/1Su2QFhXnLY',
    title: 'Reverse Engineering',
    description: `Follow along as CTF Creator Mike Lisi and James Rice solve a
                  never before seen reverse engineering CTF challenge, Go Get It.`,
    tags: ['CTF', 'Reverse Engineering', 'live']
  }
];

export const qcsPosts: MediaPostData[] = [
  {
    video: 'https://www.youtube.com/embed/MazzPR224Mg',
    title: 'Antagonizing the Red Team',
    description:
      'Questionable Cyber Strategies - Antagonizing the Red Team\n\n' +
      'Outtakes from the _2022 NCAE Cyber Competition Regionals_.',
    tags: ['Questionable Cyber Strategy', 'QCS', 'competition', 'Red Team']
  },
  {
    video: 'https://www.youtube.com/embed/XgiENR3TKO8',
    title: 'Password Fail',
    description:
      'Questionable Cyber Strategies - Password Fail\n\n' +
      'Outtakes from the _2022 NCAE Cyber Competition Regionals_.',
    tags: ['Questionable Cyber Strategy', 'QCS', 'competition']
  }
];
