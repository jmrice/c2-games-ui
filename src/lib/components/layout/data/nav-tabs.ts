import type { NavTab, NavLink } from '$lib/interfaces/navigation.interface';
import { faDiscord, faInstagram, faTwitch, faTwitter } from '@fortawesome/free-brands-svg-icons';
import type { KeycloakTokenParsed } from 'keycloak-js';

const realm = import.meta.env.VITE_KEYCLOAK_REALM;

export const registerURL = `https://auth.c2games.org/realms/${realm}/protocol/openid-connect/registrations?client_id=c2-games-ui&redirect_uri=https://www.ncaecybergames.org/user-dashboard&response_type=code`;

export function hasApiRole(token: KeycloakTokenParsed | undefined | null, role: string): boolean {
  return !!token?.['https://c2games.org/jwt/claims']?.['x-tracking-api-roles'].includes(role);
}

export function shouldDisplay(
  link: NavLink | NavTab,
  isAuthenticated: boolean | undefined,
  keycloakToken: KeycloakTokenParsed | null | undefined
) {
  // If auth is required, and we're not auth'd yet, don't display
  if (link.authRequired && !isAuthenticated) return false;
  // if a shouldDisplay function is not defined, then we always display
  if (!link.shouldDisplay) return true;
  // otherwise, check the return value of shouldDisplay
  return link.shouldDisplay(keycloakToken);
}

const navTabs: Array<NavTab | NavLink> = [
  // REGISTER
  {
    label: 'Register',
    href: registerURL,
    shouldDisplay: (token) => !token // hide when authenticated
  } as NavLink,
  // SANDBOX
  {
    label: 'Sandbox',
    links: [
      { label: 'Sandbox Tutorial Videos', href: '/tutorials' },
      { label: 'How to Get Started', target: '_blank', href: 'https://ui.sandbox.ncaecybergames.org' },
      // { label: 'DEP Challenges', href: '/sandbox-challenges' },
      // todo get sandbox URL from config, hard-coded to PROD
      { label: 'Sandbox Challenges', target: '_blank', href: 'https://ui.sandbox.ncaecybergames.org/challenges' }
    ]
  } as NavTab,
  // ABOUT
  {
    label: 'About',
    links: [
      { label: 'The Competition', href: '/about' },
      { label: 'Schedule', href: '/schedule' },
      { label: 'Rules', href: '/rules' },
      { label: 'Terms and Conditions', href: '/terms-and-conditions' }
    ]
  } as NavTab,
  // MEDIA
  {
    label: 'Media',
    href: '/about/media'
  } as NavTab,
  // STUDENTS
  {
    label: 'Students',
    authRequired: true,
    links: [
      // Top entry is an edge-case to force redirection to login page when no hash is present
      { label: 'N/a', href: '/user-dashboard', authRequired: true, shouldDisplay: () => false },
      { label: 'Account', href: '/user-dashboard#account', authRequired: true },
      {
        label: 'Team Management',
        href: '/user-dashboard#team-management',
        authRequired: true
      },
      {
        label: 'Discover Teams',
        href: '/user-dashboard#discover',
        authRequired: true
      }
    ]
  } as NavTab,
  // STAFF
  {
    label: 'Staff',
    authRequired: true,
    // only display for users with staff role
    shouldDisplay: (token) => hasApiRole(token, 'staff'),
    links: [{ label: 'Team Management', href: '/staff/team-management', authRequired: true }]
  } as NavTab,
  // HELP
  {
    label: 'Help',
    links: [
      { label: 'FAQ', href: '/faq' },
      { label: 'Contact Support', href: '/support' }
      // { label: 'Rules', href: '/rules' }
    ]
  } as NavTab,
  // CONNECT
  {
    label: 'Connect',
    links: [
      { label: 'Regional Contacts', href: '/regional-contacts' },
      { label: 'Contribute', href: '/contribute' },
      { label: 'Discord', target: '_blank', href: 'https://discord.gg/H2jwMxt', icon: faDiscord },
      { label: 'Twitch', target: '_blank', href: 'https://www.twitch.tv/ncaecybergames', icon: faTwitch },
      { label: 'Twitter', target: '_blank', href: 'https://twitter.com/ncaecybergames', icon: faTwitter },
      { label: 'Instagram', target: '_blank', href: 'https://www.instagram.com/ncaecybergames/', icon: faInstagram }
    ]
  } as NavTab
];
export default navTabs;
