---
title: '[]{#_bzm7aujw4coq .anchor}2023 NCAE Cyber Games Rules'
---

Terms
-----

Throughout these rules, the following terms are used:

-   **Red Team** - penetration testing professionals simulating external hackers attempting to gain unauthorized access to competition teams' systems.

-   **Black Team** - competition support members that provide technical support, pick-up and deliver communications, and provide overall administrative support to the competition.

-   **Blue Team/Competition Team** - the institution competitive teams consisting of students competing in a CAE event.

-   **Team Captain** - a student member of the Blue Team identified as the leader and organizer of the team.

-   **Faculty Liaison** - a faculty or staff representative of the Blue Team's host institution responsible for serving as a liaison between competition officials and the Blue Team's institution.

-   **Orange Team** - Capture The Flag (CTF) organizers responsible for developing, deploying, maintaining, and orchestrating questions regarding the CTF functionality of the event.

-   **CAE / NCAE / C2Games** - the organization responsible for producing the competition. The National Centers of Academic Excellence (NCAE/CAE) is the NSA-hosted organization that sponsors the competition.

Competitor Eligibility
----------------------

1.  Competitors in CAE events must be actively enrolled with the institution they are representing.

    1.  Team members must qualify as active part-time or full-time students as defined by the institution they are attending.

    2.  If a team member competes in a qualifying state or regional CAE event and graduates before the next CAE event in the same season, they remain eligible to participate in all events their team qualifies for within the competition season.

2.  Individual competitors may participate in CAE events for a maximum of two seasons. A CAE season is defined as the period of time between the start of the first state event and the completion of the National CAE event. Participation on a team in any CAE event during a given season counts as participation for that entire season.

3.  Competitors may only be a member of one team per CAE season.

4.  A team member may not participate in any capacity at CAE events held outside the region in which their team competes during the same CAE season.

5.  Individuals may not participate as competitors in CAE events if they are also participating in the following events during the same competition season

    1.  Regional Collegiate Cyber Defense Competition (CCDC)

    2.  National Collegiate Cyber Defense Competition (CCDC)

    3.  **Note**: Individuals not participating as competitors for the CAE event due to this ineligibility are encouraged to assist CAE participants in planning and preparation for the CAE event, but are prohibited from interacting with CAE participants in any capacity during a regional or finals event.

6.  Individuals who have participated in previous CAE events in any role other than as a competitor must obtain eligibility approval from a competition organizer prior to being added to the team roster. Once a candidate's eligibility has been approved they will remain eligible for all CAE events during the same season.

Team Composition
----------------

1.  Each team must submit a roster of at least 3 and up to 10 competitors to the designated registration system. Rosters must be submitted by published deadlines and include a faculty liaison who is a faculty member of the institution the team is representing. All competitors on the roster must meet all stated eligibility requirements.

2.  Participants who are graduate students must self-identify by notifying a Black Team member when joining a team prior to the competition date

3.  If the member of a competition team advancing to a qualifying, state, regional, or national competition is unable to attend that competition, that team may substitute another student from their institution in their place at least 1 day prior to the start of that competition.

    1.  The replacement individual must meet all eligibility requirements

    2.  The replacement is subject to approval by the Black Team and must be approved prior to the replacement participating in any event.

4.  A CAE team may only compete in one region during any given CAE season.

5.  Once a CAE event has begun, a team must complete the competition with the team that started the competition. Substitutions, additions, or removals of team members are prohibited except for extreme circumstances.

6.  Teams or team members arriving after an event's official start time, for reasons beyond their control, may be allowed to join the competition provided a substitution has not already been made. Event coordinators will review the reason for tardiness and make the final determination.

7.  Each team will designate a Team Captain for the duration of the competition to act as the official team liaison between the competition staff and the teams before and during the competition.

8.  NOTE: While the Team Captain is the official liaison between the competition staff and their team, all team members are encouraged to reach out directly to staff if and when they desire.

    1.  The number of participants per team must be distributed as equally as possible

    2.  All participants must meet eligibility criteria

    3.  All teams must meet eligibility criteria, including minimum number of participants

    4.  Teams from the same institution must all participate on the same event date

    5.  Teams from the same institution may not communicate or collaborate with each other in any way

    6.  The same faculty liaison may represent multiple teams from the same institution

9.  Exhibition teams may be approved solely at the discretion of a Competition Director

    1.  Exhibition teams may participate without meeting certain participant or team eligibility criteria

    2.  Exhibition teams are not eligible to win any CAE event and will not be considered for placement rankings in any CAE event.

    3.  Exhibition teams are not permitted to communicate or interact with any other teams during an event in any capacity

Team Representatives
--------------------

1.  Each team must have at least one faculty liaison present at every CAE event

    1.  The representative must be a faculty or staff member of the institution the team is representing.

2.  Faculty liaisons may help coach, train, and advise participants prior to live CAE events

3.  Once a CAE event has started, representatives may not coach, assist, or advise their team until the completion of that event

4.  Representatives must not interfere or interact with any other competing team.

5.  Liaisons and non-team members are prohibited from discussing any aspect of the competition event with their team during CAE competition hours and may not attempt to influence their team's performance in any way.

6.  Team representatives/coaches may not participate as a staff member or volunteer of the CAE event administration

Competition Conduct
-------------------

1.  Throughout the competition, CAE administration (excluding Red Team members) may occasionally need access to a team's system(s) for scoring, troubleshooting, etc. Teams must immediately allow access such when requested.

2.  Teams must not connect any devices or peripherals to the competition network unless specifically authorized to do so by Black Team members.

3.  In-person events:

    1.  Teams may not modify the hardware configurations of competition systems. Teams must not open the case of any server, printer, PC, monitor, KVM, router, switch, firewall, or any other piece of equipment used during the competition. All hardware related questions and issues should be referred to the Black Team.

    2.  Teams may not remove any items or equipment provided by the organization from the competition area unless specifically authorized to do so by Operations or Black Team members

4.  Team members are forbidden from entering or attempting to enter another team's competition workspace or room during CAE events.

5.  Teams are forbidden from collaborating with or communicating with other teams in any way other than public Discord channels specifically provisioned by CAE organizers.

6.  Teams must compete without "outside assistance" from non-team members including team representatives from the start of the competition to the end of the competition. All private communications (calls, emails, chat, texting, directed emails, forum postings, conversations, requests for assistance, etc) with non-team members including team representatives that would help the team gain an unfair advantage are not allowed and are grounds for disqualification and/or a penalty assigned to the appropriate team.

7.  Printed reference materials (books, magazines, checklists) are permitted in competition areas and teams may bring printed reference materials to the competition.

8.  Team representatives, sponsors, and observers are not competitors and are prohibited from directly assisting any competitor through direct advice, "suggestions", or hands-on assistance. Any team sponsor or observers found assisting a team will be asked to leave the competition area for the duration of the competition and/or a penalty will be assigned to the appropriate team.

9.  Teams are free to examine their own systems, but no offensive activity against any system outside the team\'s assigned network(s), including those of other CAE teams, will be tolerated. Any team performing offensive activity against any system outside the team\'s assigned network(s) will be immediately disqualified from the competition. If there are any questions or concerns during the competition about whether or not specific actions can be considered offensive in nature, contact the Operations Team before performing those actions.

10. Teams are allowed to use active response mechanisms such as TCP resets when responding to suspicious/malicious activity. Any active mechanisms that interfere with the functionality of the scoring engine or manual scoring checks are exclusively the responsibility of the teams. Any firewall rule, IDS, IPS, or defensive action that interferes with the functionality of the scoring engine or manual scoring checks are exclusively the responsibility of the teams.

Internet Usage
--------------

1.  Public internet resources such as FAQs, how-to\'s, existing forums and responses, and company websites, are completely valid for competition use provided there is no fee required to access those resources and access to those resources has not been granted based on a previous membership, purchase, or fee.

2.  Only resources that could reasonably be available to all teams are permitted

    1.  For example, creating a personal public GitHub repository would be prohibited, as no other team would reasonably be able to access the repository.. Only public resources that every team could access if they chose to are permitted.

3.  Teams may not use any external, private electronic staging area or FTP site for patches, software, etc. during the competition.

    1.  Teams are not allowed to access private Internet-accessible libraries, FTP sites, web sites, network storage, email accounts, or shared drives during the competition.

    2.  All Internet resources used during the competition must be freely available to all other teams.

    3.  The use of external collaboration and storage environments such as Google Docs/Drive is prohibited unless the environment was provided by and is administered by competition officials.

    4.  Accessing private staging areas or email accounts is grounds for disqualification and/or a penalty assigned to the appropriate team.

4.  No peer to peer or distributed file sharing clients or servers are permitted on competition networks unless specifically authorized by the competition officials.

5.  Internet activity, where allowed, will be monitored and any team member caught viewing inappropriate or unauthorized content will be subject to disqualification and/or a penalty assigned to the appropriate team. This includes direct contact with outside sources through AIM/chat/email or any other public or non-public services including sites such as Facebook. For the purposes of this competition inappropriate content includes pornography or explicit materials, pirated media files, sites containing key generators and pirated software, etc. If there are any questions or concerns during the competition about whether or not specific materials are unauthorized contact the Black Team immediately.

6.  All network activity that takes place on the competition network may be logged and subject to release. Competition officials are not responsible for the security of any information, including login credentials, which competitors place on the competition network.

7.  Scripts, executables, tools, and programs written by active team members may be used in CAE events provided:

    1.  The scripts, executables, tools, and programs have been published as a publicly available resource on a public and non-university affiliated site such as GitHub or SourceForge for at least 3 months prior to their use in any CAE event.

    2.  Teams must send the public links and descriptions of the team-written scripts, executables, tools, and programs to the appropriate competition director at least 30 days prior to their use in any CAE event. Development must be "frozen" at time of submission with no modifications or updates until after the team competes in their last CAE event of that season.

    3.  Teams must consent to the distribution of the submitted links and descriptions to all other teams competing in the same CAE event where the team-written scripts, executables, tools, and programs will be used.

    4.  Team written tools, scripts, or executables that transmit data outside of the competition environment (such as log data) must be declared to competition officials at least 30 days prior to their use in any CAE event. Teams must obtain written authorization from competition officials prior to using these tools in any CAE event. Approval or rejection of these tools is at the sole discretion of competition officials.

Permitted Materials
-------------------

1.  No memory sticks, flash drives, removable drives, CDROMs, electronic media, or other similar electronic devices are allowed in the room during the competition unless specifically authorized by the Operations or Black Team in advance. Any violation of these rules will result in disqualification of the team member and/or a penalty assigned to the appropriate team.

2.  Teams may not bring any type of computer, laptop, or tablet into the competition area unless specifically authorized by the Operations or White Team in advance. Any violation of these rules will result in disqualification of the team member and/or a penalty assigned to the appropriate team.

3.  Printed reference materials (books, magazines, checklists) are permitted in competition areas and teams may bring printed reference materials to the competition as specified by the competition officials.

Professional Conduct
--------------------

1.  All individuals involved with the CAE event in any capacity are expected to behave professionally at all times during all CAE events including preparation meetings, receptions, mixers, banquets, competitions and so on.

2.  For in-person events, in addition to published CAE rules, Host Site policies and rules apply throughout the competition and must be respected by all CAE participants. It is the responsibility of the team to be aware of the Host Site policies and rules.

3.  All CAE events are alcohol free events. No alcohol consumption is permitted at any time during competition hours.

4.  Activities such as swearing, consumption of alcohol or illegal drugs, disrespectful or unruly behavior, sexual harassment, improper physical contact, becoming argumentative, willful violence, or willful physical damage have no place at the competition and will not be tolerated.

5.  Violations of the rules can be deemed unprofessional conduct if determined to be intentional or malicious by competition officials.

6.  Competitors behaving in an unprofessional manner may receive a warning from the CAE administration, or for more egregious actions and subsequent violations following a warning, competitors may have a penalty assessed against their team, be disqualified, expelled from the competition site, and/or be subject to applicable law enforcement. Competitors expelled for unprofessional conduct will be banned from future CAE competitions in perpetuity at the sole discretion of a Competition Director.

7.  Individual(s), other than competitors, behaving in an unprofessional manner may be warned against such behavior by the Competition Staff or asked to leave the competition entirely by a Competition Director.

Questions, Disputes, and Disclosures
------------------------------------

1.  PRIOR TO THE COMPETITION: Team captains are encouraged to work with the Competition Director and their staff to resolve any questions regarding the rules of the competition or scoring methods before the competition begins.

2.  DURING THE COMPETITION: Protests by any team must be presented in writing by the Team Captain as soon as possible. The competition officials will be the final arbitrators for any protests or questions arising before, during, or after the competition. Rulings by the competition officials are final

3.  All competition results are official and final as of the Closing Ceremony.

4.  In the event of an individual disqualification, that team member must leave the competition area immediately upon notification of disqualification and must not re-enter the competition area at any time. Disqualified individuals are also ineligible for individual or team awards.

5.  In the event of a team disqualification, the entire team must leave the competition area immediately upon notice of disqualification and is ineligible for any individual or team award.

Scoring
-------

1.  Scoring will be based on keeping required services available and functional, controlling/preventing unauthorized access, and solving CTF challenges. Teams accumulate points by successfully completing challenges and maintaining services. Teams lose points by violating service level agreements.

2.  Competition officials may utilize "Bonus" or "Other" points at their sole discretion. These points may be used to penalize teams, or compensate for unexpected circumstances during a CAE event.

3.  Scores will be maintained by the competition officials and may be shared at the end of the competition. Running totals provided throughout the competition are for informational purposes only. Scores are not considered final until announced at the closing ceremony by a competition official.

4.  Any team action that interrupts the scoring system is exclusively the responsibility of that team and will result in a lower score. Any team member that modifies a competition system or system component, with or without intent, in order to mislead the scoring engine into assessing a system or service as operational, when in fact it is not, may be disqualified and/or the team assessed penalties. Should any question arise about scoring, the scoring engine, or how scoring functions, the Team Captain should immediately contact the competition officials to address the issue.
