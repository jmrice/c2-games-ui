export interface LightboxData {
  title?: string;
  src?: string;
}
