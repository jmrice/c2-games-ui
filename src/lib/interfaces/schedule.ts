import type { Event } from '../util/apiHelpers';

export type GroupedEventsByMonth = {
  // Key here is month as a string
  [key: string]: GroupedEventsByDay;
};

export type GroupedEventsByDay = {
  // key here is day of the month as a number
  [key: number]: Event[];
};
