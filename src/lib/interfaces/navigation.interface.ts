import type { IconDefinition } from '@fortawesome/fontawesome-common-types';
import type { KeycloakTokenParsed } from 'keycloak-js';

export type shouldDisplayFn = (keycloak: KeycloakTokenParsed | null | undefined) => boolean;

export interface NavLink {
  href: string;
  label: string;
  authRequired?: boolean;
  shouldDisplay?: shouldDisplayFn;
  target?: '_blank' | '_self' | '_parent' | '_top' | string;
  icon?: IconDefinition;
}

export interface NavTab {
  label: string;
  href?: string;
  links: NavLink[];
  authRequired?: boolean;
  shouldDisplay?: shouldDisplayFn;
}

export function isNavLink(obj: unknown): obj is NavLink {
  return !!(Object.prototype.hasOwnProperty.call(obj, 'label') && Object.prototype.hasOwnProperty.call(obj, 'href'));
}

export function isNavTab(obj: unknown): obj is NavTab {
  return !!(Object.prototype.hasOwnProperty.call(obj, 'label') && Object.prototype.hasOwnProperty.call(obj, 'links'));
}
