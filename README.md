# c2-games-ui

## Developing

Note: project developed using Node v16.14.0

Once you've cloned the project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Environment variables

You will need the following environment variables in a `.env` file at the project root:

```
# Vars for Keycloak auth
VITE_KEYCLOAK_URL=https://auth.c2games.org
VITE_KEYCLOAK_REALM=dev
VITE_KEYCLOAK_CLIENT_ID=c2-games-ui

# URL for backend API
VITE_API_URL=http://localhost:5000
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Updating the Rules

To update the rules markdown file, download the Google Doc as a docx, and run this pandoc command:

```
pandoc -s Rules.docx --wrap=none --reference-links -t markdown -o src/lib/assets/content/rules.md
```
