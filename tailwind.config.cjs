/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      backgroundImage: {
        main: "url('/main_background.png')",
        binary: "url('/binary_bg.png')"
      },
      colors: {
        'cyber-blue': {
          500: '#368DE3',
          600: '#106097',
          800: '#193d66',
          900: '#0C2856'
        },
        'cyber-gray': {
          100: '#FEFEFE',
          200: '#F2F2F2',
          300: '#D5D5D5',
          500: '#BFC7CB',
          600: '#74879C',
          800: '#333333'
        },
        secondary: '#E3574D'
      },
      fontSize: {
        xxl: ['16rem', '1']
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
        'nunito-sans': ['"Nunito Sans"']
      }
    }
  },
  safelist: [
    'hidden',
    // Terms / rules pages (rendered from markdown)
    'list-decimal',
    'list-inside',
    'pl-5',
    {
      // text size classes for markdown
      pattern: /text-([0-9]?).*/,
      variants: ['md']
    },
    {
      // padding and margin classes
      pattern: /[p,m][tblr]?-[0-9]+/
    },
    {
      // color classes because some are used dynamically (ex, <Alert.svelte>)
      pattern:
        /(bg|border|text)-(slate|gray|zinc|neutral|stone|red|orange|amber|yellow|lime|green|emerald|teal|cyan|sky|blue|indigo|violet|purple|fuchsia|pink|rose|cyber-blue|cyber-gray|secondary)-[1-9]00/,
      variants: ['dark']
    },
    {
      // width/height classes because some are used dynamically (ex, <Spinner.svelte>)
      pattern: /(w|h)-([0-9]+)/
    }
  ],
  plugins: []
};
